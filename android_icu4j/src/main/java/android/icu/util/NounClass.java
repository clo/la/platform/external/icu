/* GENERATED SOURCE. DO NOT MODIFY. */
// © 2022 and later: Unicode, Inc. and others.
// License & terms of use: http://www.unicode.org/copyright.html

package android.icu.util;

/**
 * Represents all the grammatical noun classes that are supported by CLDR.
 *
 * @hide Only a subset of ICU is exposed in Android
 * @hide draft / provisional / internal are hidden on Android
 */
public enum NounClass {
    /**
     * @hide draft / provisional / internal are hidden on Android
     */
    OTHER,
    /**
     * @hide draft / provisional / internal are hidden on Android
     */
    NEUTER,
    /**
     * @hide draft / provisional / internal are hidden on Android
     */
    FEMININE,
    /**
     * @hide draft / provisional / internal are hidden on Android
     */
    MASCULINE,
    /**
     * @hide draft / provisional / internal are hidden on Android
     */
    ANIMATE,
    /**
     * @hide draft / provisional / internal are hidden on Android
     */
    INANIMATE,
    /**
     * @hide draft / provisional / internal are hidden on Android
     */
    PERSONAL,
    /**
     * @hide draft / provisional / internal are hidden on Android
     */
    COMMON,
}
